const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

async function checkEmailExists(req, res) {
  try {
    const result = await User.find({ email: req.body.email });
    if (result.length > 0) {
      return res.send(true);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function registerUser(req, res) {
  const hashedPassword = bcrypt.hashSync(req.body.password, 10);
  try {
    const newUser = new User({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email,
      mobileNo: req.body.mobileNo,
      password: hashedPassword,
    });
    const result = await newUser.save();
    if (result) {
      return res.send(true);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function loginUser(req, res) {
  try {
    const user = await User.findOne({ email: req.body.email });
    if (user == null) {
      return res.send(false);
    }
    const isPasswordCorrect = bcrypt.compareSync(
      req.body.password,
      user.password
    );
    if (isPasswordCorrect) {
      return res.send({ access: auth.createAccessToken(user) });
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function getProfile(req, res) {
  // console.log('you here')
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  try {
    // const user = await User.findById(req.body.id);
    const user = await User.findById(userData.id);
    if (user) {
      user.password = "******";
      return res.send(user);
    }
    return res.send("no user associated with that ID");
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

async function enrollUser(req, res) {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);

  if(userData.isAdmin){
    return res.send('You are not allowed to enroll in a course!');
  }

  let data = {
    userId: req.body.userId,
    courseId: req.body.courseId,
  };
  
  try {
    let user = await User.findById(data.userId);
    if (!user) {
      return res.send(false);
    }
    console.log(user);

    user.enrollments.push({ courseId: data.courseId });
    const userEnrollmentResult = await user.save();

    if (!userEnrollmentResult) {
      return res.send(false);
    }
    let course = await Course.findById(data.courseId);

    if (!course) {
      res.send(false);
    }
    course.enrollees.push({ userId: data.userId });
    const updateCourseResult = await course.save();

    if (userEnrollmentResult && updateCourseResult) {
      return res.send("User successfuly enrolled");
    }
    return res.send("client cannot be enrolled. error encountered!");
  } catch (error) {
    console.log(error);
    res.send(error);
  }
}

module.exports = {
  checkEmailExists: checkEmailExists,
  registerUser: registerUser,
  loginUser: loginUser,
  getProfile: getProfile,
  enrollUser: enrollUser,
};
