const Course = require("../models/Course");
const auth = require("../auth");

async function addCourse(req, res) {
  const userData = auth.decode(req.headers.authorization);
  console.log(userData);
  if (!userData.isAdmin) {
    return res.send(
      "You are not an admin. You are not allowed to create a course "
    );
  }

  let newCourse = new Course({
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  });
  try {
    const course = await newCourse.save();
    if (course) {
      return res.send(`Course ${req.body.name} created`);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function getAllCourses(req, res) {
  try {
    const courses = await Course.find({});

    if (courses) {
      return res.send(courses);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function getAllActive(req, res) {
  try {
    const courses = await Course.find({ isActive: true });
    if (courses && courses.length > 0) {
      return res.send(courses);
    }
    return res.send(false);
  } catch {
    console.log(error);
    return res.send(error);
  }
}

async function getTheCourse(req, res) {
  try {
    const course = await Course.findById(req.params.courseId);
    if (course) {
      return res.send(course);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function updateTheCourse(req, res) {
  const userData = auth.decode(req.headers.authorization);

  const updatedCourse = {
    name: req.body.name,
    description: req.body.description,
    price: req.body.price,
  };
  try {
    if (!userData.isAdmin) {
      return res.send(
        "You are not an admin! You are not required to update a course"
      );
    }
    const result = await Course.findByIdAndUpdate(
      req.params.courseId,
      updatedCourse
    );

    if (result) {
      return res.send(updatedCourse);
    }
    return res.send(false);
  } catch (error) {
    console.log(error);
    return res.send(error);
  }
}

async function archiveTheCourse(req, res) {
  const userData = auth.decode(req.headers.authorization);

  try {
    if (!userData.isAdmin) {
      return res.send(
        "You are not an admin! You are not required to archive a course"
      );
    }
    const result = await Course.findByIdAndUpdate(req.params.courseId, {
      isActive: req.body.isActive,
    });

    if (result) {
      return res.send(true);
    }
    return res.send(false);
  } catch {
    console.log(error);
    return res.send(error);
  }
}

module.exports = {
  addCourse: addCourse,
  getAllCourses: getAllCourses,
  getAllActive: getAllActive,
  getTheCourse: getTheCourse,
  updateTheCourse: updateTheCourse,
  archiveTheCourse: archiveTheCourse,
};
