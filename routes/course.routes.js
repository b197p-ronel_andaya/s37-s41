const express = require('express');
const courseController = require('../controllers/courseController');
const auth = require('../auth');
const router = express.Router();

router.post('/',auth.verify,courseController.addCourse);
router.get('/all', auth.verify,courseController.getAllCourses);
router.get('/',courseController.getAllActive);
router.get('/:courseId',courseController.getTheCourse);
router.put('/:courseId',auth.verify,courseController.updateTheCourse);
router.put('/:courseId/archive',auth.verify,courseController.archiveTheCourse);

module.exports = router;