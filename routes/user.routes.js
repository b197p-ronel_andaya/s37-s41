const express = require("express");
const userController = require("../controllers/userController");
const auth = require("../auth");

const router = express.Router();

router.post("/checkEmail", userController.checkEmailExists);
router.post("/register", userController.registerUser);
router.post("/login", userController.loginUser);
router.get("/details", auth.verify, userController.getProfile);
router.post('/enroll', auth.verify,userController.enrollUser);

module.exports = router;
