const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://motifaithed:pass1234@zuittbootcampb197-p.m5646t2.mongodb.net/s37-s41?retryWrites=true&w=majority`,{
    useNewUrlParser:true,
    useUnifiedTopology: true
})

const dbConnect = mongoose.connection;

dbConnect.on('error',()=>console.error('Connection Error'));
dbConnect.once('open',()=>console.log('Connected to MongoDB'));

module.exports = dbConnect;