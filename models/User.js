const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  firstName: {
    type: String,
    required: [true, "First Name required"],
  },
  lastName: {
    type: String,
    required: [true, "Last Name required"],
  },
  email: {
    type: String,
    required: [true, "Email required"],
  },
  password: {
    type: String,
    required: [true, "Password required"],
  },
  isAdmin: {
    type: Boolean,
    default: false,
  },
  mobileNo: {
    type: String,
    required: [true, "Mobile No. required"],
  },
  enrollments: [
    {
      courseId: {
        type: String,
        required: [true, "Course ID required"],
      },
      enrolledOn: {
        type: Date,
        default: new Date(),
      },
      status: {
        type: String,
        default: "Enrolled",
      },
    },
  ],
});

module.exports = mongoose.model("User", userSchema);
