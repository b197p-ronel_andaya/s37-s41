const express = require('express');
const cors = require('cors');
const userRoutes = require('./routes/user.routes');
const courseRoutes = require('./routes/course.routes')
const port = process.env.PORT || 4000;

// if(process.env.PORT){
//     port = process.env.PORT;
// }
const app = express();

const db = require('./data/database');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/users',userRoutes);
app.use('/courses', courseRoutes);

app.listen(port,()=>{
console.log(`API is now online at port: ${port}`);
})